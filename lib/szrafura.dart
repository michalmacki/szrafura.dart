@JS()
library szrafura;

import "package:js/js.dart";
import 'dart:html';

@JS("_szrafura")
external CanvasPattern szrafura(String kolor, String wzor);


@JS("_szrafuraObrazkowa")
external CanvasPattern szrafuraObrazkowa(String kolor,
    List<String> sciezki, int szerokosc, int wysokosc, [bool czyWypelnic= false]);
