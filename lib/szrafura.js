﻿/*
Funkcja do budowy szrafur
@param String kolor Kolor w formacie HEX lub RGB
@param String wzor Jeden z: '=', '||', '#', '/', 'x', 'o'
@return CanvasPattern
 */
/*CanvasPattern*/ function _szrafura(/*String*/ kolor, /*String*/ wzor) {
    /*CanvasElement*/ var _canvas = document.createElement('canvas');
    _canvas.width = 32;
    _canvas.height = 32;
    /*CanvasRenderingContext2D*/ var _kontekst = _canvas.getContext('2d');
    _kontekst.strokeStyle = kolor;
    _kontekst.lineWidth = 1;
    switch(wzor) {
        case '=':
            _kontekst.moveTo(0,8);
            _kontekst.lineTo(32,8);
            _kontekst.stroke();
            _kontekst.moveTo(0,24);
            _kontekst.lineTo(32,24);
            _kontekst.stroke();
            break;
        case '||':
            _kontekst.moveTo(8,0);
            _kontekst.lineTo(8,32);
            _kontekst.stroke();
            _kontekst.moveTo(24,0);
            _kontekst.stroke();
            break;
        case '#':
            _kontekst.moveTo(8,0);
            _kontekst.lineTo(8,32);
            _kontekst.stroke();
            _kontekst.moveTo(24,0);
            _kontekst.lineTo(24,32);
            _kontekst.stroke();
            _kontekst.moveTo(0,8);
            _kontekst.lineTo(32,8);
            _kontekst.stroke();
            _kontekst.moveTo(0,24);
            _kontekst.lineTo(32,24);
            _kontekst.stroke();
            break;
        case 'x':
            _kontekst.lineTo(32,32);
            _kontekst.stroke();
            _kontekst.moveTo(0,32);
            _kontekst.lineTo(32,0);
            _kontekst.stroke();
            break;
        case '/':
            _kontekst.lineTo(32,32);
            _kontekst.stroke();
            break;
        case 'o':
            _kontekst.arc(16,16,4,0,Math.PI*2);
            _kontekst.stroke();
            break;
    }
    return _kontekst.createPattern(_canvas, 'repeat');
}

/*
Funkcja do budowy wypełnień ze ścieżek SVG
@param String Kolor HEX lub RGB
@param List<String> Lista ścieżek SVG
@param int szerokosc Szerokość układu współrzędnych SVG
@param int wysokosc Wysokość układu współrzędnych SVG
@return CanvasPattern
 */
function _szrafuraObrazkowa(/*String*/ kolor, /*List<String>*/ sciezki, /*int */szerokosc, /*int*/ wysokosc, /*bool*/czyWypelnic) {
    /*CanvasElement*/ var _canvas = document.createElement('canvas');
    _canvas.width = szerokosc;
    _canvas.height = wysokosc;
    /*CanvasRenderingContext2D*/ var _kontekst = _canvas.getContext('2d');
    _kontekst.strokeStyle = kolor;
    for(var i=0;i<sciezki.length;i++) {
        var sciezka = sciezki[i];
        _kontekst.beginPath();
        _kontekst.stroke(new Path2D(sciezka));
        if(czyWypelnic === true) {
            _kontekst.fill(new Path2D(sciezka));
        }
    }
    return _kontekst.createPattern(_canvas, 'repeat');
}